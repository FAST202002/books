![]( https://www.futureschool.cn/WWWSCHOOL/upload/images/2020/2/268e2865b7339a7e.png )

------------

# about future academy of science and technology

![]( Https://www.futureschool.cn/WWWSCHOOL//upload/images/2021/3/1534a7f5830853ea.jpg )

Shanghai Pudong future science and technology school is an innovative school jointly organized by Pearl future education and Boston Institute of future education, supported by HTH, the top innovation school in the United States. The school adheres to the school running philosophy of "Chinese culture, world vision, humanistic feeling and science and technology", hoping to cultivate a number of "world vision and patriotism"; Knowledge, scientific and technological literacy; Outstanding talents with physical and mental health and responsibility.

Located in Zhoupu intelligent industrial park, Pudong New Area, the school is adjacent to Shanghai Disneyland, metro line 18 and Shanghai South Road provide extremely convenient traffic conditions. The beautiful campus with complete facilities nestles in Wanda Square and Zhoupu old street. Modernization and historical feeling reflect each other, which provides a good learning environment for the healthy growth of teenagers.

![]( https://www.futureschool.cn/WWWSCHOOL//upload/images/2021/3/afc552ee5a23574e.jpg )

>Boston Future Education Research Institute was founded by Qibu future education group in Boston, United States New England Higher Education Commission, Harvard University Education College and Zhangjiang Boston enterprise park. It is led by many Nobel Prize winners and academicians to explore the mode of innovative education. It provides strong support for the innovation curriculum research and development of Pudong future science and technology school and the students' promotion to the University in the United States.

>The New England Higher Education Commission (nebhe) is a regional higher education organization authorized by the United States Congress and approved by the governors and legislative bodies of the six states of New England. The group represents 260 institutions of higher learning in New England, including Harvard, Yale, Dartmouth, brown and MIT. Half of the Ivy League school in the United States, the two best liberal arts colleges in the United States: Williams College and amerst college are also in the New England area. In addition, New England has Tufts University, Brandis University, University of Connecticut, Boston University, Boston College, Worcester Institute of technology and other top 50 universities in the United States.

------------

# school running features

### It is a school that lays a solid foundation for children

The primary school adheres to the 30 years practical experience of "intelligent education" of Mr. Ni Hua, the special president of Shanghai. The junior high school emphasizes the curriculum teaching concept of "mathematics leading, English speaking and advancing together and developing in an all-round way". The teaching is led by Shanghai special level teachers and discipline experts to ensure the high quality implementation of nine-year compulsory education curriculum.

### This is a school with scientific innovation characteristics

The school and HTH, the top innovation school in the United States, have worked in depth to create a museum like school for children. The project based learning (PBL) is adopted to guide students to pay attention to reality and find problems. Under the support of theory and data, students explore and practice feasible solutions independently, and finally clearly show the learning results. Cultivate children's interest and enthusiasm in learning, master the thinking habits and basic abilities of solving problems in the future.

### This is a school that doesn't have written homework to go home

In order to "liberate parents", and to better cultivate students' conscious learning habits, the school arranged teachers as tutors for late study, timely helped and tutored students in their daily work, and strive to "not go home without writing". The school has a language support center for foreign teachers to teach English. Students can learn and communicate in a pure English environment every day, which is helpful to help each student learn the second language effectively and effectively, and lay a good foundation for the cultivation of international talents.

------------
# link area

[official website of the school](https://www.futureschool.cn/WWWSCHOOL/index.html ) [中文](https://www.luogu.com.cn/user/515011)
